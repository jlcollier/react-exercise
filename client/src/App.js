import React, { Component } from 'react';
import { Dropdown } from 'react-dropdown-now';
import ClipLoader from "react-spinners/ClipLoader";
import 'react-dropdown-now/style.css';
import './App.css';

const representativeList = {
  "Senator":"senators",
  "Representative":"representatives"
};

const stateList = {
  "Alabama":"AL","Alaska":"AK","Arizona":"AZ","Arkansas":"AR","California":"CA",
  "Colorado":"CO","Connecticut":"CT","Delaware":"DE","Florida":"FL","Georgia":"GA",
  "Hawaii":"HI","Idaho":"ID","Illinois":"IL","Indiana":"IN","Iowa":"IA",
  "Kansas":"KS","Kentucky":"KY","Louisiana":"LA","Maine":"ME","Maryland":"MD",
  "Massachusetts":"MA","Michigan":"MI","Minnesota":"MN","Mississippi":"MS","Missouri":"MO",
  "Montana":"MT","Nebraska":"NE","Nevada":"NV","New Hampshire":"NH","New Jersey":"NJ",
  "New Mexico":"NM","New York":"NY","North Carolina":"NC","North Dakota":"ND","Ohio":"OH",
  "Oklahoma":"OK","Oregon":"OR","Pennsylvania":"PA","Rhode Island":"RI","South Carolina":"SC",
  "South Dakota":"SD","Tennessee":"TN","Texas":"TX","Utah":"UT","Vermont":"VT",
  "Virginia":"VA","Washington":"WA","West Virginia":"WV","Wisconsin":"WI","Wyoming":"WY"
};

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      ableToSearch: false,
      searchOffice: null,
      searchState: null,
      resultsLoading: false,
      results: null,
      resultsOfficeType: null,
      selectedOfficer: null
    }

    this.setOfficeSelection = this.setOfficeSelection.bind(this);
    this.setStateSelection = this.setStateSelection.bind(this);
    this.checkAndSetAbilityToSearch = this.checkAndSetAbilityToSearch.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onShowOfficer = this.onShowOfficer.bind(this);
  }

  setStateSelection(value) {
    this.checkAndSetAbilityToSearch(value.value, 'state');
    this.setState({ searchState: value.value });
  }

  setOfficeSelection(value) {
    this.checkAndSetAbilityToSearch(value.value, 'office');
    this.setState({ searchOffice: value.value });
  }

  checkAndSetAbilityToSearch(value, valueType) {
    if (valueType === 'state') {
      if(value && this.state.searchOffice) {
        this.setState({ ableToSearch: true });
      }
    } else if (valueType === 'office'){
      if (value && this.state.searchState) {
        this.setState({ ableToSearch: true });
      }
    }
  }

  onSearchSubmit() {

    this.setState({
      resultsLoading: true,
      resultsOfficeType: this.state.searchOffice,
      selectedOfficer: null
    });

    const officeUrlParam = representativeList[this.state.searchOffice];
    const stateUrlParam = stateList[this.state.searchState];

    fetch('http://localhost:3001/' + officeUrlParam + '/' + stateUrlParam, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
    .then(response => response.json())
    .then(response => {
      console.log(response);
      if (response.results) {
        this.setState({
          resultsLoading: false,
          results: response.results
        });
      } else {
        this.setState({
          resultsLoading: false,
          results: null,
          resultsOfficeType: null
        });
      }
    })
    .catch((error) => {
      this.setState({
        resultsLoading: false,
        results: null,
        resultsOfficeType: null
      });
      console.error('An issue happened on the fetch: ', error);
      alert('There was an error processing your request. Please refresh the page or contact your administrator.');
    });
  }



  onShowOfficer(officer) {
    this.setState({ selectedOfficer: officer });
  }

  render() {

    const {
      ableToSearch,
      resultsLoading,
      results,
      resultsOfficeType,
      selectedOfficer,
    } = this.state;

    const list = results || [];

    return (
      <div className="app">
        <div className="app-content">
          <h2>Who's My Representative?</h2>
          <div className="selection-interface">
            <Dropdown
              placeholder="Select an Office"
              className="officer-dropdown"
              options={Object.keys(representativeList)}
              value=""
              onSelect={(value) => this.setOfficeSelection(value)}
            />
            <Dropdown
              placeholder="Select a State"
              className="state-dropdown"
              options={Object.keys(stateList)}
              value=""
              onSelect={(value) => this.setStateSelection(value)}
            />
            { ableToSearch
              ? <button className="button"
                onClick={this.onSearchSubmit}
              >
                Search
              </button>
              : <div>
              </div>
            }
          </div>
          <div></div>
          { results
            ? <div className="results">
              <div className="results-list-table">
                {list.map(rep =>
                  <div key={rep.phone} className="table-row" onClick={() => this.onShowOfficer(rep)}>
                    <span style={{ width: '70%' }}>
                      {rep.name}
                    </span>
                    <span style={{ width: '30%' }}>
                      {rep.party.substring(0,1)}
                    </span>
                  </div>
                )}
              </div>
              { selectedOfficer
                ? <div className="individual-info">
                  <div className="individual-info-field">First Name: {selectedOfficer.name.split(' ')[0]}</div>
                  <div className="individual-info-field">Last Name: {selectedOfficer.name.split(' ').pop()}</div>
                  {selectedOfficer.district
                    ?<div className="individual-info-field">District: {selectedOfficer.district}</div>
                    : null
                  }
                  <div className="individual-info-field">Phone number: {selectedOfficer.phone}</div>
                  <div className="individual-info-field">Office: {resultsOfficeType}</div>
                </div>
                : null
              }
            </div>
            : null
          }
        </div>
        { resultsLoading
          ? <div className="spinner">
            <ClipLoader color={"#000000"} loading={resultsLoading} css={null} size={150}/>
          </div>
          : null
        }
      </div>
    )
  }
}

export default App;
